#!/usr/bin/env bash

set -Eeu


mode=$1
postfix="${mode//-/_}"
  
if [[ -z ${EOS_ACCOUNT_PASSWORD+x} || -z ${EOS_ACCOUNT_USERNAME+x} || -z ${EOS_ACCOUNT_PATH+x} ]]; then 
    echo "eos env not defined, cannot validate ntuple"
    exit 1
fi
 
if [[ -z "${CI_MERGE_REQUEST_IID:-}" ]]; then
    echo "merge request ID not defined, cannot validate ntuple"
    exit 1
fi

if [[ -z "${VALIDATION_REFERENCE_MR:-}" ]]; then
    echo "reference merge request not defined, cannot validate ntuple"
    exit 1
fi

eosplotdir=$EOS_ACCOUNT_PATH/gitlab_ci_output/plots/$CI_MERGE_REQUEST_IID/$postfix
eosfile=$EOS_ACCOUNT_PATH/gitlab_ci_output/ntuples/$CI_MERGE_REQUEST_IID/analysis_${postfix}.root
refeosdir=$EOS_ACCOUNT_PATH/gitlab_ci_output/ntuples/$VALIDATION_REFERENCE_MR
refeosfile=$refeosdir/analysis_${postfix}.root

echo $EOS_ACCOUNT_PASSWORD | kinit -l 1h $EOS_ACCOUNT_USERNAME 
klist

if [[ ! -r ${eosfile} ]]; then
    echo "$eosfile is not readable"
    exit 1
fi

if [[ ! -r ${refeosfile} ]]; then
    echo "$eosfile is not readable"
    exit 1
fi

mkdir -p $eosplotdir
echo "compareOutputNtuples root://eosuser.cern.ch//$refeosfile root://eosuser.cern.ch//$eosfile"
compareOutputNtuples root://eosuser.cern.ch//$refeosfile root://eosuser.cern.ch//$eosfile 2>&1 | tee branch-diff-log.txt
echo "comparing ntuples succeeded"

echo $CI_MERGE_REQUEST_TITLE > $eosplotdir/CI_MERGE_REQUEST_TITLE
echo $VALIDATION_REFERENCE_MR > $eosplotdir/VALIDATION_REFERENCE_MR
cat $refeosdir/CI_MERGE_REQUEST_TITLE > $eosplotdir/VALIDATION_MERGE_REQUEST_TITLE
echo $CI_COMMIT_MESSAGE > $eosplotdir/CI_COMMIT_MESSAGE
echo $CI_COMMIT_SHA > $eosplotdir/CI_COMMIT_SHA
echo $CI_COMMIT_SHORT_SHA > $eosplotdir/CI_COMMIT_SHORT_SHA

cp branch-diff-log.txt $eosplotdir
if [[ ! -z "$(ls -A compare_output/)" ]]; then
    echo "compare_output folder not empty, copying figures to eos"
    cp compare_output/* $eosplotdir
fi

ndiff=`\grep  -- '----> .* branches are different'  branch-diff-log.txt | awk '{print $2}'`
nadd=`\grep  -- '----> .* branches are added'  branch-diff-log.txt | awk '{print $2}'`
nrm=`\grep  -- '----> .* branches are removed' branch-diff-log.txt | awk '{print $2}'`

exitcode=0
if [[ ! "$nadd" == 0 ]]; then
    echo "$nadd branches are added, please check branch-diff-log"
    exitcode=24
fi
if [[ ! "$nrm" == 0 ]]; then
    echo "$nrm branches are removed, please check branch-diff-log"
    exitcode=23
fi
if [[ ! "$ndiff" == 0 ]]; then
    echo "$ndiff branches are different, please check plots"
    exitcode=22
fi
echo "$ndiff branches are different, $nrm are removed, and $nadd are added" > $eosplotdir/CI_VAL_SUMMARY
echo $exitcode > $eosplotdir/CI_VAL_EXIT_CODE
echo "===== ls $eosplotdir ====="
ls $eosplotdir

echo "exit $exitcode"

exit $exitcode
