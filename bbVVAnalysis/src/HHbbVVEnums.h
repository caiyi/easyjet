#ifndef BBVVANALYSIS_HHBBVVENUMS
#define BBVVANALYSIS_HHBBVVENUMS

namespace HHBBVV
{

  enum Channel
  {
    Boosted1Lep = 0,
    SplitBoosted1Lep = 1,
    Boosted0Lep = 2,
    SplitBoosted0Lep = 3,
  };
}

#endif